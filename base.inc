<h2>Get involved</h2>

<p>Join the Merlin community, by joining our mailing list, checking
out our code and filing merge requests and bugs.</p>

<ul>
<li>Mailing list: <a href="mailto:merlin.discuss@librelist.com">merlin.discuss@librelist.com</a></li>
<li><a href="http://github.com/baughj/Merlin/issues">Issue tracker</a></li>
<li><a href="/community/">Get involved</a></li>
<li><a href="/code/">Get the code</a></li>
</ul>

</div>
</div>
<div id="ft">

<p class="c">Merlin is written by Justin Baugh, and is free
software,<br />licensed under the <a
href="http://www.gnu.org/licenses/agpl.html">GNU Affero General Public
License</a>, version 3 or later.</p>

<p class="c"><a href="http://identi.ca/merlincloud">Follow us on
identi.ca</a> | <a href="http://twitter.com/merlincloud">Follow us on
Twitter</a></p>

<p>The Merlin logo is Creative Commons Attribution-ShareAlike 3.0
licensed, by <a href="http://matt.lee.name/">Matt Lee</a>, with
contributions from the public domain Open Clip Art library.</p>
    
</div>
</div>
</body>
</html>