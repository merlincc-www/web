#!/bin/bash

# Merlin: free software for your cloud
# Copyright (C) 2010, 2011 Justin Baugh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Some settings
HTMLDIR=./public_html
TMPDIR=./tmp
GIT_BRANCH='master'
SMARTYPANTS=SmartyPants.pl
HEADER=./top.inc
FOOTER=./base.inc

# Update from git
cd $HTMLDIR && git pull origin $GIT_BRANCH

# Clean out temporary directory

rm -rf $TMPDIR
mkdir $TMPDIR

# Process all the markdown files

for x in `find . -name "*.mdwn"`;
do
    DIR=`dirname $x`
    BASE=`basename $x .mdwn`
    cat $HEADER > $TMPDIR/$BASE.html
    markdown $x >> $TMPDIR/$BASE.html
    cat $FOOTER >> $TMPDIR/$BASE.html
    # Run the entire thing through SmartyPants
    $SMARTYPANTS $TMPDIR/$BASE.html > $TMPDIR/$BASE.sp.html
    mv $TMPDIR/$BASE.sp.html $DIR/$BASE.html

done
