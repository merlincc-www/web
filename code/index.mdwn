# Get the code!

## Gitorious

Gitorious is the primary development location for Merlin:

* git clone git://gitorious.org/merlincc/mainline.git

## GitHub

In order to work well with people who are using GitHub, we also mirror
Merlin on GitHub:

* git clone git://github.com/baughj/Merlin.git

