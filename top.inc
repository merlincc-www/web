<!DOCTYPE html>
<html>
<head>
<title>Merlin: free software Ruby on Rails cloud controller</title>
   <link rel="stylesheet" href="/combo.css" type="text/css">
   <link rel="stylesheet" href="/merlin.css" type="text/css">

</head>
<body>
<div id="custom-doc" class="yui-t7">
   <div id="hd"><h1><a href="/">Merlin</a><span>: free software Ruby on Rails cloud controller</span></h1>

     <ul>
       <li><a href="/code">Install Merlin now! <b>&rarr;</b></a></li>
     </ul>

   </div>
   <div id="bd">
	<div class="yui-g">

        <p id="banner"><img height="230" src="/i/front.png" alt="Take control of your clouds, create instances and snapshots and provision new instances with ease" /></p>

